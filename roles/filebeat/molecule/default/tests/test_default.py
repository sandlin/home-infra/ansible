import testinfra
import testinfra.utils.ansible_runner
import os

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_filebeat_package_installed(host):
    assert host.package("filebeat").is_installed


def test_filebeat_command_executes(host):
    assert host.run("filebeat --version").rc == 0


def test_filebeat_config_files_exist(host):
    assert host.file('/etc/filebeat/filebeat.yml').exists


def test_filebeat_config_is_valid(host):
    assert host.run("filebeat test config").rc == 0


def test_filebeat_service_is_enabled(host):
    assert host.service('filebeat').is_enabled
