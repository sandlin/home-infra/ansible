# __PROJECT NAME__

![Project Stage][project-stage-shield]
![Maintenance][maintenance-shield]
![Pipeline Status][pipeline-shield]

[![License][license-shield]](LICENSE.md)
[![Maintaner][maintainer-shield]](https://gitlab.com/jsandlin)

## Table of Contents
[[_TOC_]]

## Requirements
A file `~/.ansible/vault.pass`. The value is stored in MyPassword.
```
  ansible-galaxy install -r roles/requirements.yml
```
Install `sshpass`
```shell
brew install sshpass
brew install esolitos/ipa/sshpass
```
### MacOS
1. `gnu-tar`
  - `brew install gnu-tar`
1. You may see
```
url lookup connecting to https://github.com/prometheus/node_exporter/releases/download/v1.1.2/sha256sums.txt
objc[67335]: +[__NSCFConstantString initialize] may have been in progress in another thread when fork() was called.
objc[67335]: +[__NSCFConstantString initialize] may have been in progress in another thread when fork() was called. We cannot safely call it or ignore it in the fork() child process. Crashing instead. Set a breakpoint on objc_initializeAfterForkError to debug.
ERROR! A worker was found in a dead state
```

This is apparently due to some new security changes made in High Sierra that are breaking lots of Python things that use fork(). Rumor has it that adding
```
export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES
```
before your Ansible run should clear it up. The code that’s causing issues is well below Ansible in the stack.

If so, workaround is:
`export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES`
- REF: https://fantashit.com/osx-crash-complaining-of-operation-in-progress-in-another-thread-when-fork-was-called/
- [Demo cloudalchemy Site](https://github.com/cloudalchemy/demo-site)

Apps / ports
| App | Port | 
| --- | --- |
| [Grafana](http://dell.h8n.lan:3000) | 3000 | 
| [Node Exporter](http://dell.h8n.lan:9100) | 9100 |
| [Prometheus UI](http://dell.h8n.lan:9090)| 9090 |
| [snmp_exporter](dell:9116) | 9116 |
| blackbox_exporter | 9115 |
| [alertmanager](dell.h8n.lan:9093) | 9093 |



# Prometheus Queries
1. Get all series in DB with data
   ```shell
   {__name__!=""}
   ```
2. Get all metric names
   ```
   group by(__name__)({__name__!=""})
   ```
3. Get all series for a metrics
   - Choose a name from above.
     ```shell
     node_disk_io_now
     ```
4. All metrics of one box
   ```shell
    group by(__name__) ({instance="box.foo.lan"})
   ```
5. Get all values for specific label / device
   - All metrics (IOW: List all instances)
     ```shell
     group by(instance) ({__name__!=""})
     ```
   - Single metric
     ```shell
     group by(instance) (go_info)
     ```
6. Performance Queries
   - Number of series per metric name
     ```shell
     sort_desc(count by(__name__) ({__name__!=""}))
     ```
   - Number of series per target
     ```shell
     sort_desc(count by(instance) ({__name__!=""}))
     ```
   - Number of series per job & metric
     ```shell
     sort_desc(count by(job, __name__) ({__name__!=""}))
     ```
   - Number of values in specific bucket under specific metric
     ```shell
     count(group by(job) (go_info))
     ```
```
snmpwalk -v 2c -c public 10.0.0.1
promql 'up{}' | jq .

```
## Usage

#### Node Exporter
`ansible-playbook node_exporter.yml`
1. node_exporter.yml
  - Deploy prometheus node exporter using ansible.
  - [SRC](https://galaxy.ansible.com/cloudalchemy/node_exporter)
  - [CloudAlchemy/ansible-prometheus](https://github.com/cloudalchemy/ansible-prometheus/blob/400b7e010951ea6de4fdf2a835c21ab71f983c28/tasks/preflight.yml)
  - [CloudAlchemy/ansible-grafana](https://github.com/cloudalchemy/ansible-grafana)
  - [modules](https://github.com/prometheus/node_exporter#enabled-by-default)
#### snmp-exporter
[source](https://performance-monitoring-with-prometheus.readthedocs.io/en/latest/switch.html#snmp-exporter)
Prometheus provides official SNMP support through snmp_exporter, which consist of:
  - *exporter*: collect metrics from managed devices through SNMP, acts as a NMS;
  - *generator*: create configurations for exporter by mapping SNMP OIDs to counters, gauges which can be understood by Prometheus;
  
[REF](https://galaxy.ansible.com/cloudalchemy/snmp-exporter)
[SNMP-EXPORTER for network equipment](https://performance-monitoring-with-prometheus.readthedocs.io/en/latest/switch.html)
---
# References
- [PromQL Queries for Exploring Metrics](https://promlabs.com/blog/2020/12/17/promql-queries-for-exploring-your-metrics)
- 

<!-- Let's define some variables for ease of use. -->
[project-stage-shield]: https://img.shields.io/badge/project%20stage-development-yellowgreen.svg
[issue]: https://gitlab.com/sandlin/project_templates/common/issues
[license-shield]: https://img.shields.io/github/license/hassio-addons/repository-beta.svg
[maintenance-shield]: https://img.shields.io/maintenance/yes/2021.svg
[maintainer-shield]: https://img.shields.io/badge/maintainer-James_Sandlin-blueviolet
[pipeline-shield]: https://gitlab.com/sandlin/project_templates/common/groot/pipeline.svg
[pipeline-status]: https://gitlab.com/sandlin/project_templates/common/badges/groot/pipeline.svg
[code-coverage]: https://gitlab.com/sandlin/project_templates/common/badges/groot/coverage.svg
